## 0.0.1 (2020-11-22)

### Features

- `slurp`: download the closed captions from a YouTube channel's uploads
- `train`: train a neural network based on data from `emacnn slurp`
- `vlog`: generate a video script
