# EmacNN
EmacNN is a CLI for training a neural network to generate YouTube video scripts using YouTube's automated closed captions for a specific channel's videos.

The name, Emac, refers ostensibly to one of [Eric McHenry's]((https://www.youtube.com/c/GodofKings1)) preferred nicknames. His long, rambling, often circuitous, and long ["vlogs"]((https://www.youtube.com/c/GodofKings1)) inspired this project.

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/tedtramonte/emacnn)](https://gitlab.com/tedtramonte/emacnn/builds)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/emacnn)](https://pypi.org/project/emacnn/)
[![PyPI - License](https://img.shields.io/pypi/l/emacnn)](https://choosealicense.com/licenses/mit/)
[![PyPI - Version](https://img.shields.io/pypi/v/emacnn)](https://gitlab.com/tedtramonte/emacnn/-/releases)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/emacnn)](https://gitlab.com/tedtramonte/emacnn)
[![PyPI - Wheel](https://img.shields.io/pypi/wheel/emacnn)](https://gitlab.com/tedtramonte/emacnn)
[![PyPI - Status](https://img.shields.io/pypi/status/emacnn)](https://gitlab.com/tedtramonte/emacnn)

## Installation
The best way to install EmacNN is to use Pip.

```bash
pip install emacnn
```

## Usage
```bash
# Display help text
emacnn --help
```

## Contributing
Merge requests are welcome after opening an issue first. Please make sure to update tests as appropriate.

### Development
Install in a virtual environment with:

```bash
python -m venv env
pip install -e .
```

To improve model training time, it is reocmmended to use CUDA if your GPU is supported.
1. Refer to the [Keras requirements](https://www.tensorflow.org/install/gpu).
2. [Install CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit-archivE) matching Keras requirement
3. [Install cuDNN](https://developer.nvidia.com/rdp/cudnn-archive) matching Keras requirement
