__title__ = 'EmacNN'
__author__ = 'Ted Tramonte'
__license__ = 'MIT'
__copyright__ = 'Copyright 2020 Ted Tramonte'

# from .cli import emac
from emacnn import data
from emacnn import features
from emacnn import model

# flake8: noqa
