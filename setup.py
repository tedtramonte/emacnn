import setuptools

requirements = []
with open('requirements.txt') as f:
    requirements = f.read().splitlines()

readme = ''
with open('README.md') as f:
    readme = f.read()

setuptools.setup(
    name='emacnn',
    author='Ted Tramonte',
    url='https://gitlab.com/tedtramonte/emacnn',
    project_urls={
        "Issue tracker": "https://gitlab.com/tedtramonte/emacnn/issues",
    },
    use_scm_version=True,
    packages=setuptools.find_packages(),
    license='MIT',
    description='A CLI for training a neural network on a specific YouTube channel\'s videos.',
    long_description=readme,
    long_description_content_type="text/markdown",
    setup_requires=[
        'setuptools_scm'
    ],
    include_package_data=True,
    install_requires=requirements,
    entry_points='''
    [console_scripts]
    emacnn=emacnn.cli:emac
    ''',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: End Users/Desktop',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Topic :: Games/Entertainment',
        'Topic :: Scientific/Engineering :: Artificial Intelligence',
        'Topic :: Sociology',
    ]
)
